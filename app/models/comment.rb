class Comment < ActiveRecord::Base
	validates :post_id, presence: true
	belongs_to :post, dependent: :destroy
end
